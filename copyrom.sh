#!/bin/bash

# Commodore's firmware is owned and licensed by Cloanto. The ROMs required by VICE can be obtained as part of Cloanto's C64 Forever Express https://www.c64forever.com/download/
# For convenience Linux users are advised to extra C64 Forever using msitools's msiextract command.
# After installation, locate /Program Files/Cloanto/C64 Forever/Shared directory, which should contain the rom subdirectory. 
# Download this file to Shared and run it with sudo to copy all ROMS, with their expected names, to /usr/share/vice/

#C64 firmware
cp rom/c-64-kernal.rom /usr/share/vice/C64/kernal
cp rom/c-64-chargen.rom /usr/share/vice/C64/chargen
cp rom/c-64-basic.rom /usr/share/vice/C64/basic
cp rom/c-64-kernal-jp.rom /usr/share/vice/C64/jpkernal
cp rom/c-64-chargen-jp.rom /usr/share/vice/C64/jpchrgen

#C128 firmware
cp rom/c-128-basic-64.rom /usr/share/vice/C128/basic64
cp rom/c-128-basic-hi.rom /usr/share/vice/C128/basichi
cp rom/c-128-basic-lo.rom /usr/share/vice/C128/basiclo
cp rom/c-128-chargen.rom /usr/share/vice/C128/chargen
cp rom/c-128-chargen-ch.rom /usr/share/vice/C128/chargch
cp rom/c-128-chargen-de.rom /usr/share/vice/C128/chargde
cp rom/c-128-chargen-fr.rom /usr/share/vice/C128/chargfr
cp rom/c-128-chargen-no.rom /usr/share/vice/C128/chargno
cp rom/c-128-chargen-se.rom /usr/share/vice/C128/chargse
cp rom/c-128-kernal-64.rom /usr/share/vice/C128/kernal64
cp rom/c-128-kernal.rom /usr/share/vice/C128/kernal
cp rom/c-128-kernal-ch.rom /usr/share/vice/C128/kernalch
cp rom/c-128-kernal-de.rom /usr/share/vice/C128/kernalde
cp rom/c-128-kernal-fi.rom /usr/share/vice/C128/kernalfi
cp rom/c-128-kernal-fr.rom /usr/share/vice/C128/kernalfr
cp rom/c-128-kernal-it.rom /usr/share/vice/C128/kernalit
cp rom/c-128-kernal-no.rom /usr/share/vice/C128/kernalno
cp rom/c-128-kernal-se.rom /usr/share/vice/C128/kernalse

#VIC20 firmware
cp rom/c-vic20-basic.rom /usr/share/vice/VIC20/basic
cp rom/c-vic20-chargen.rom /usr/share/vice/VIC20/chargen
cp rom/c-vic20-kernal.rom /usr/share/vice/VIC20/kernal

#PLUS4 firmware
cp rom/c-plus4-kernal.rom /usr/share/vice/PLUS4/kernal
cp rom/c-16-kernal.rom /usr/share/vice/PLUS4/kernal.005
cp rom/c-232-kernal.rom /usr/share/vice/PLUS4/kernal.232
cp rom/c-v364-kernal.rom /usr/share/vice/PLUS4/kernal.364
cp rom/c-plus4-ext-hi.rom /usr/share/vice/PLUS4/3plus1hi
cp rom/c-plus4-ext-lo.rom /usr/share/vice/PLUS4/3plus1lo
cp rom/c-plus4-basic.rom /usr/share/vice/PLUS4/basic
cp rom/c-v364-kernal.rom /usr/share/vice/PLUS4/c2lo.364

#C64DTV firmware
cp rom/c-64-kernal.rom /usr/share/vice/C64DTV/kernal #check
cp rom/c-64-chargen.rom /usr/share/vice/C64DTV/chargen #check
cp rom/c-64-basic.rom /usr/share/vice/C64DTV/basic #check

#SCPU64 firmware
cp rom/c-64-chargen.rom /usr/share/vice/SCPU64/chargen #check
cp rom/c-64-chargen-jp.rom /usr/share/vice/SCPU64/jpchrgen #check
# Vice's replacement SCPU64 ROM "compatible with the original to avoid distribution problems" is used here so a licensed original is not required.

#CBM-II firmware
cp rom/c-500-basic.rom /usr/share/vice/CBM-II/basic.500
cp rom/c-500-chargen.rom /usr/share/vice/CBM-II/chargen.500
cp rom/c-500-kernal.rom /usr/share/vice/CBM-II/kernal.500
cp rom/c-700-kernal.rom /usr/share/vice/CBM-II/kernal
cp rom/c-600-128k-basic.rom /usr/share/vice/CBM-II/basic.128
cp rom/c-600-256k-basic.rom /usr/share/vice/CBM-II/basic.256
cp rom/c-600-chargen.rom /usr/share/vice/CBM-II/chargen.600
cp rom/c-600-kernal.rom /usr/share/vice/CBM-II/kernal.600
cp rom/c-700-chargen.rom /usr/share/vice/CBM-II/chargen.700
cp rom/c-700-kernal.rom /usr/share/vice/CBM-II/kernal.700

#PET firmware - not currently available so we're building dummy files that can be replaced later. PET emulation will obviously not work with these dummy files in place.
touch /usr/share/vice/PET/basic1
touch /usr/share/vice/PET/basic2
touch /usr/share/vice/PET/basic4
touch /usr/share/vice/PET/characters.901640-01.bin
touch /usr/share/vice/PET/chargen
touch /usr/share/vice/PET/chargen.de
touch /usr/share/vice/PET/edit1g
touch /usr/share/vice/PET/edit2b
touch /usr/share/vice/PET/edit2g
touch /usr/share/vice/PET/edit4b40
touch /usr/share/vice/PET/edit4b80
touch /usr/share/vice/PET/edit4g40
touch /usr/share/vice/PET/hre-9000.324992-02.bin
touch /usr/share/vice/PET/hre-a000.324993-02.bin
touch /usr/share/vice/PET/kernal1
touch /usr/share/vice/PET/kernal2
touch /usr/share/vice/PET/kernal4
touch /usr/share/vice/PET/waterloo-a000.901898-01.bin
touch /usr/share/vice/PET/waterloo-b000.901898-02.bin
touch /usr/share/vice/PET/waterloo-c000.901898-03.bin
touch /usr/share/vice/PET/waterloo-d000.901898-04.bin
touch /usr/share/vice/PET/waterloo-e000.901897-01.bin
touch /usr/share/vice/PET/waterloo-f000.901898-05.bin

#drive firmware
cp rom/c-drive-1541-ii.rom /usr/share/vice/DRIVES/d1541II
cp rom/c-drive-1571-c128dcr.rom /usr/share/vice/DRIVES/d1571cr
cp rom/c-drive-1001.rom /usr/share/vice/DRIVES/dos1001
cp rom/c-drive-1541.rom /usr/share/vice/DRIVES/dos1540 #this is a cludge to make it build as we don't have the 1540 firmware. This placeholder will probably break things if used.
cp rom/c-drive-1541.rom /usr/share/vice/DRIVES/dos1541
cp rom/c-drive-1551.rom /usr/share/vice/DRIVES/dos1551
cp rom/c-drive-1570.rom /usr/share/vice/DRIVES/dos1570
cp rom/c-drive-1571.rom /usr/share/vice/DRIVES/dos1571
cp rom/c-drive-1581.rom /usr/share/vice/DRIVES/dos1581
cp rom/c-drive-2031.rom /usr/share/vice/DRIVES/dos2031
cp rom/c-drive-2040.rom /usr/share/vice/DRIVES/dos2040
cp rom/c-drive-3040.rom /usr/share/vice/DRIVES/dos3040
cp rom/c-drive-4000.rom /usr/share/vice/DRIVES/dos4000
cp rom/c-drive-4040.rom /usr/share/vice/DRIVES/dos4040
cp rom/c-drive-1581.rom /usr/share/vice/DRIVES/dos1581
touch /usr/share/vice/DRIVES/dos9000 #dos9000 image is missing from Cloanto set. this is a placeholder and will break horribly if used

#printer firmware
cp rom/c-printer-mps801.rom /usr/share/vice/PRINTER/mps801
cp rom/c-printer-mps802.rom /usr/share/vice/PRINTER/cbm1526
cp rom/c-printer-mps803.rom /usr/share/vice/PRINTER/mps803
cp rom/c-printer-mps803.rom /usr/share/vice/PRINTER/nl10-cbm #nl10-cbm printer firmware is created & owned by Star. this is a placeholder and will probably break horribly if used
